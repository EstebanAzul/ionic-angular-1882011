import { Receta } from './receta.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recetas',
  templateUrl: './recetas.page.html',
  styleUrls: ['./recetas.page.scss'],
})
export class RecetasPage implements OnInit {
    recetas: Receta[] = [
      {id:1, titulo:'Pizza', imageUrl:'https://www.dondeir.com/wp-content/uploads/2019/08/pizza-hut-cadenas-de-pizza-cdmx.jpg',
      ingredientes: ['pan','queso']

      }
    ]

  constructor() { }

  ngOnInit() {
  }

}
